// Create the Express app
const express = require("express");
const app = express();
const path = require('path');
const bodyParser = require('body-parser')
const url = require('url');   
const queryString = require('query-string');
const validate = require('./contract.js');
const Base64 = require('js-base64').Base64;
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

// Enable static access to the "/public" folder
app.use(express.static('form'))
app.use(express.static('contractformat'))
app.use(express.static('error'))

let port = process.argv[2];
if (!port) port = process.env['PORT'];
if (!port) port = 3000;


function convertToArrayOfObjects(data,currency) {
	var output = [];
    for (var i = 0; i < data.length; i++) {
    	if(data[i][0]){
	    	var obj = 
	    		{
					description : data[i][0],
					quantity	: data[i][1],
					quantity_price : data[i][2],
					price_currency : currency
				};	
        output.push(obj);
    	}
    }
    return output;
}

app.get('/upload', (req,res) =>{
	res.sendFile(path.join(__dirname + '/form/index.html'),);
});

const asyncMiddleware = fn =>
  (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next);
  };

app.post('/upload', asyncMiddleware(async function(req, res, next){
	const { buyer_fullname,buyer_email,buyer_address,service,transaction_currency } = req.body;
	const date_ob = new Date();
	var services = convertToArrayOfObjects(service,transaction_currency);
	let upload = {
		header : "Services Agreement",
		state : "Taiwan",
		date : {
			day:date_ob.getDate(),
			month:date_ob.getMonth() + 1 ,
			year: date_ob.getFullYear()
		},
		seller : {
			fullname:'Ecloudture' ,
			address:'7F., No.111, Sec. 4, Sanhe Rd., Sanchong Dist., New Taipei City 24152, Taiwan'
		},
		buyer : {
			fullname: buyer_fullname ,
			address: buyer_address
		},
		services : services,

		total_fee : '$1,000.00',
		tax_by_buyer : true,

		payment : {
			method:'cash',
			data:[
				{_class:'prepaid_payment',amount:'10.00'},
				{_class:'lease_incentive',amount:'100.00'},
				{_class:'closing_payment',amount:'10.00'}		
			]
		},

		inspection :{
			rights : true,
			deadline : 10 ,
			options : {
				revision : 0,
				terminate : '50%'
			}
		}
	}
  	await validate.upload(JSON.stringify(upload));
  	let hash = await validate.upload(JSON.stringify(upload));
  	upload.contract_hash = hash.txhash;
	let buff = Base64.encode(encodeURIComponent(JSON.stringify(upload)));
	res.redirect('/contract?valid=' + buff);
}));

function safeJSON (json) {
  var parsed
  try {
    parsed = JSON.parse(json)
  } catch (e) {
    app.get('/error', (req, res) =>{
    	res.sendFile(path.join(__dirname + '/error/'))
    })
  }
  return parsed 
}

app.get('/contract',  (req, res) =>{
	var passedVariable = decodeURIComponent(Base64.decode(req.query.valid));
	let useful = safeJSON(passedVariable);
    console.log(useful);
	res.sendFile(path.join(__dirname + '/contractformat/contractformat.html'),{
      upload : useful,
    });
});

function safelyParseJSON (json) {
  var parsed
  try {
    parsed = JSON.parse(json)
    delete parsed.contract_hash;
  } catch (e) {
    app.get('/error', (req, res) =>{
    	res.sendFile(path.join(__dirname + '/error/index.html'))
    })
  }
  return parsed 
}

app.get('/validation', asyncMiddleware(async function(req, res, next){
  var passedVariable = decodeURIComponent(Base64.decode(req.query.valid));
  console.log(passedVariable);
  var parser = safelyParseJSON(passedVariable);
  console.log(parser);
  let responz = await validate.verify(JSON.stringify(parser));
  console.log(responz);
  if(responz.error){
  	res.sendFile(path.join(__dirname + '/error/index.html'))
  }
  else{
    console.log(responz.containing);
  	res.sendFile(path.join(__dirname + '/contractformat/contractformat.html'),{
      upload : JSON.parse(responz.containing)
    });
  }  
}))


app.listen(port, () => {
	console.log(`Server started on port : ${port}!`);
});

